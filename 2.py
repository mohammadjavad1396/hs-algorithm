import json
import socket
import time
from threading import Thread


def listen(node):
    while True:
        c, addr = node.server.accept()
        kwargs = json.loads(c.recv(1024), encoding='utf-8')
        received_index = kwargs['index']
        received_message = kwargs['message'].split()
        received_uid = int(received_message[0])
        received_flag = received_message[1]
        received_phase = int(received_message[2])
        send_minus = None
        send_plus = None
        if received_flag == 'out':
            print(f"Node {node.uid} received from node {received_uid}:"
                  f" ELECTION {max(received_uid, node.uid)}")
            if node.uid == received_uid:
                print(f"Node {node.uid} received from node {received_uid}: LEADER ----{node.uid}")
                send_plus = (node.uid, 'end', 1)
                send_minus = (node.uid, 'end', 1)

            elif node.uid < received_uid:
                if received_phase != 1:
                    if received_index == node.minus_index:
                        send_plus = (received_uid, 'out', received_phase - 1)
                    elif received_index == node.plus_index:
                        send_minus = (received_uid, 'out', received_phase - 1)

                elif received_phase == 1:
                    if received_index == node.minus_index:
                        send_minus = (received_uid, 'in', 1)
                    elif received_index == node.plus_index:
                        send_plus = (received_uid, 'in', 1)

        elif received_flag == 'in':
            print(f"Node {node.uid} received from node {received_uid}:"
                  f" ELECTION {max(received_uid, node.uid)}")
            if received_uid != node.uid:
                if received_index == node.minus_index:
                    send_plus = (received_uid, 'in', 1)
                elif received_index == node.plus_index:
                    send_minus = (received_uid, 'in', 1)
            else:
                node.phase += 1
                send_plus = (node.uid, 'out', 2 ** node.phase)
                send_minus = (node.uid, 'out', 2 ** node.phase)

        elif received_flag == 'end':
            print(f"Node {node.uid} found leader: LEADER {received_uid}")
            if received_index == node.minus_index:
                send_plus = (received_uid, 'end', 1)
            elif received_index == node.plus_index:
                send_minus = (received_uid, 'end', 1)

        if send_minus:
            msg_minus = {'index': node.index, 'message': f'{send_minus[0]} {send_minus[1]} {send_minus[2]}'}
            byte_array_minus = json.dumps(msg_minus).encode('utf-8')
            node._send_with_delay(byte_array_minus, 'minus')
            if send_minus[1] == 'end':
                break

        if send_plus:
            msg_plus = {'index': node.index, 'message': f'{send_plus[0]} {send_plus[1]} {send_plus[2]}'}
            byte_array_plus = json.dumps(msg_plus).encode('utf-8')
            node._send_with_delay(byte_array_plus, 'minus')
            if send_plus[1] == 'end':
                break


class Node:
    def __init__(self, *args, **kwargs):
        self.status = 'unknown'
        self.phase = 1
        self.queue_size = 10
        self.uid = kwargs['uid']
        self.delay = kwargs['delay']
        self.index = kwargs['index']
        self.address = kwargs['address']
        self.port = kwargs['port']
        self.minus_index = kwargs['minus_index']
        self.minus_address = kwargs['minus_address']
        self.minus_port = kwargs['minus_port']
        self.plus_index = kwargs['plus_index']
        self.plus_address = kwargs['plus_address']
        self.plus_port = kwargs['plus_port']

        self._init_server()

    def _init_server(self):
        s = socket.socket()
        s.bind((self.address, self.port))
        s.listen(self.queue_size)
        self.server = s

    def start(self):
        msg = {'index': self.index,
               'message': f'{self.uid} out {1}'}
        byte_array = json.dumps(msg).encode('utf-8')
        self._send_with_delay(byte_array, 'minus')
        self._send_with_delay(byte_array, 'plus')

    def _send_with_delay(self, byte_array, direction):
        connection = socket.socket()
        if direction == 'minus':
            connection.bind((self.address, 0))
            connection.connect((self.minus_address, self.minus_port))
        elif direction == 'plus':
            connection.bind((self.address, 0))
            connection.connect(((self.plus_address, self.plus_port)))

        time.sleep(self.delay)
        connection.send(byte_array)
        connection.close()


if __name__ == '__main__':

    base_port = 5000
    n = int(input())
    node_info = []
    for i in range(n):
        node_info.append(list(map(int, input().split())))

    nodes = [None] * n
    nodes[0] = Node(uid=node_info[0][0],
                    delay=node_info[0][1],
                    index=1,
                    address='localhost',
                    port=base_port + 1,
                    minus_index=0,
                    minus_address='localhost',
                    minus_port=base_port + n,
                    plus_index=2,
                    plus_address='localhost',
                    plus_port=base_port + 2,
                    )

    nodes[-1] = Node(uid=node_info[-1][0],
                     delay=node_info[-1][1],
                     index=n,
                     address='localhost',
                     port=base_port + n,
                     minus_index=1,
                     minus_address='localhost',
                     minus_port=base_port + n - 1,
                     plus_index=1,
                     plus_address='localhost',
                     plus_port=base_port + 1,
                     )

    for i in range(1, n - 1):
        index = i + 1
        nodes[i] = Node(uid=node_info[i][0],
                        delay=node_info[i][1],
                        index=index,
                        address='localhost',
                        port=base_port + index,
                        minus_index=index - 1,
                        minus_address='localhost',
                        minus_port=base_port + index - 1,
                        plus_index=index + 1,
                        plus_address='localhost',
                        plus_port=base_port + index + 1,
                        )

    for node in nodes:
        t = Thread(target=listen, args=(node,))
        t.start()

    for node in nodes:
        node.start()
